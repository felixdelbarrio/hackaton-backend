package com.example.hackaton.back.services;


import com.example.hackaton.back.models.AccountModel;
import com.example.hackaton.back.repositories.AccountRepository;
import com.example.hackaton.back.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    UserRepository userRepository;

    public AccountServiceResponse add (AccountModel account){
        System.out.println("add");

        AccountServiceResponse result = new AccountServiceResponse();
        result.setAccount(account);
        if (this.userRepository.findById(account.getIdUser()).isEmpty()){
            System.out.println("El usuario no existe por lo que no se puede crear la cuenta");
            result.setMsg("El usuario no existe por lo que no se puede crear la cuenta");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            return result;
        }
        if (this.accountRepository.findById(account.getAccount()).isPresent()){
            System.out.println("Ya hay una cuenta con este identificador");
            result.setMsg("Ya hay una cuenta con este identificador");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            return result;
        }
        this.accountRepository.save(account);
        result.setMsg("Cuenta creada correctamente");
        result.setHttpStatus(HttpStatus.CREATED);
        return result;
    }

    public List<AccountModel> findAll(){
        System.out.println("findAll");
        return this.accountRepository.findAll();
    }

    public Optional<AccountModel> findById (String id){
        System.out.println("findById");
        System.out.println("Obteniendo la cuenta con la id "+id);
        return this.accountRepository.findById(id);
    }
    public boolean delete (String id){
        System.out.println("removeById");
        if (this.accountRepository.findById(id).isPresent()){
            this.accountRepository.deleteById(id);
            return true;
        }
        return false;
    }
    public AccountServiceResponse update (String id, AccountModel updateAccount){
        System.out.println("update");
        AccountServiceResponse result = new AccountServiceResponse();
        if (updateAccount.getAccount().equals(id)) {
            Optional<AccountModel> res = findById(id);
            if (res.isPresent()) {
                System.out.println("La cuenta a actualizar existe");
                result.setAccount(updateAccount);
                if (this.userRepository.findById(updateAccount.getIdUser()).isEmpty()) {
                    System.out.println("El usuario no existe por lo que no se puede crear la cuenta");
                    result.setMsg("El usuario no existe por lo que no se puede crear la cuenta");
                    result.setHttpStatus(HttpStatus.BAD_REQUEST);
                    return result;
                }
                this.accountRepository.save(updateAccount);
                result.setMsg("Cuenta modificada correctamente");
                result.setHttpStatus(HttpStatus.CREATED);
                return result;
            } else {
                result.setMsg("Cuenta no existente");
                result.setHttpStatus(HttpStatus.NOT_FOUND);
                return result;
            }
        }else {
            result.setMsg("No coinciden la Cuenta de la URL y la del Body");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            return result;
        }
    }

    public AccountServiceResponse partialUpdate (String id, AccountModel account){
            System.out.println("partialUpdate");
            AccountServiceResponse result = new AccountServiceResponse();
            if (account.getAccount().equals(id)) {
                Optional<AccountModel> accountInDB = findById(id);
                if (accountInDB.isPresent()) {
                    System.out.println("La cuenta existe");
                    result.setAccount(account);
                    if (account.getIdUser() != null) {
                        result.setMsg("No está permitido actualizar el usuario de la cuenta");
                        result.setHttpStatus(HttpStatus.BAD_REQUEST);
                        return result;
                    }
                    if (account.getAmount() != 0) {
                        result.setMsg("No está permitido actualizar la cantidad");
                        result.setHttpStatus(HttpStatus.BAD_REQUEST);
                        return result;
                    }
                    if (account.getMovements() != null) {
                        Float acumulado = accountInDB.get().getAmount();
                        if (accountInDB.get().getMovements() != null) {
                            System.out.println("Actualizando movimientos");
                            Map<String, Float> movements = accountInDB.get().getMovements();
                            for (Map.Entry<String, Float> movItem : account.getMovements().entrySet()) {
                                if (this.accountRepository.findById(movItem.getKey()).isEmpty()) {
                                    System.out.println("La cuenta " + movItem.getKey() + " no existe");
                                    result.setMsg("La cuenta " + movItem.getKey() + " no existe");
                                    result.setHttpStatus(HttpStatus.BAD_REQUEST);
                                    return result;
                                } else {
                                    System.out.println("Añadiendo movimiento desde " + movItem.getKey() + " a la cuenta " + accountInDB.get().getAccount());
                                    acumulado += movItem.getValue();
                                    movements.put(movItem.getKey(), movItem.getValue());
                                }
                            }
                            accountInDB.get().setAmount(acumulado);
                            accountInDB.get().setMovements(movements);
                        } else {
                            for (Map.Entry<String, Float> movItem : account.getMovements().entrySet()) {
                                acumulado += movItem.getValue();
                            }
                            accountInDB.get().setAmount(acumulado);
                            accountInDB.get().setMovements(account.getMovements());
                        }
                    }
                    this.accountRepository.save(accountInDB.get());
                    result.setMsg("Cuenta actualizada correctamente");
                    result.setHttpStatus(HttpStatus.OK);
                    return result;
                }
                result.setMsg("Cuenta no encontrada");
                result.setHttpStatus(HttpStatus.NOT_FOUND);
                return result;
            }else{
                result.setMsg("No coinciden la Cuenta de la URL y la del Body");
                result.setHttpStatus(HttpStatus.BAD_REQUEST);
                return result;
            }
        }
}
