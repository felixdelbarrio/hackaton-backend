package com.example.hackaton.back.services;

import com.example.hackaton.back.models.AccountModel;
import org.springframework.http.HttpStatus;

public class AccountServiceResponse {
    private String msg;
    private AccountModel account;
    private HttpStatus httpStatus;

    public AccountServiceResponse() {
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public AccountModel getAccount() {
        return this.account;
    }

    public void setAccount(AccountModel purchase) {
        this.account = purchase;
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
