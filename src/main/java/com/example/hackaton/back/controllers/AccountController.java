package com.example.hackaton.back.controllers;


import com.example.hackaton.back.models.AccountModel;
import com.example.hackaton.back.services.AccountService;
import com.example.hackaton.back.services.AccountServiceResponse;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/v1/accounts")
@CrossOrigin(origins = "*",
        methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
public class AccountController {

    @Autowired
    AccountService accountService;

    @GetMapping
    public ResponseEntity<List<AccountModel>> getAccounts(){
        System.out.println("getAccounts");
        return new ResponseEntity<>(this.accountService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public  ResponseEntity<Object> getAccountById(@PathVariable String id){
        System.out.println("getAccountById");
        System.out.println("id es "+id);
        Optional<AccountModel> result = this.accountService.findById(id);
        return new ResponseEntity<>(result.isPresent() ? result.get() : "Cuenta no encontrado", result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<AccountServiceResponse> addAccount(@RequestBody AccountModel newAccount){
        System.out.println("Creando nueva cuenta");
        System.out.println("La nueva cuenta es: "+newAccount.getAccount());
        System.out.println("El id usuario asociado a la cuenta es: "+newAccount.getIdUser());
        System.out.println("La cantidad con la que se crea la cuenta: "+newAccount.getAmount());
        AccountServiceResponse result = this.accountService.add(newAccount);
        return new ResponseEntity<>(result,result.getHttpStatus());
    }

    @PutMapping("/{accountId}")
    public ResponseEntity<AccountServiceResponse> updateAccount(@PathVariable String accountId, @RequestBody AccountModel account){
        System.out.println("updateAccount");
        System.out.println("La cuenta recibida por parametro es: " + accountId);
        System.out.println("La cuenta a atualizar es: " + account.getAccount());
        System.out.println("El id de usuario es: " + account.getIdUser());
        System.out.println("La cantidad a actualizar es: " + account.getAmount());
        System.out.println("Los movimientos a actualizar son: " + account.getMovements());
        AccountServiceResponse result = this.accountService.update(accountId, account);
        return new ResponseEntity<>(result,result.getHttpStatus());
    }

    @PatchMapping("/{accountId}")
    public ResponseEntity<Object> patchAccount (@PathVariable String accountId, @RequestBody AccountModel account){
        System.out.println("patchProduct");
        System.out.println("La cuenta recibida por parametro es: " + accountId);
        System.out.println("La cuenta a atualizar es: " + account.getAccount());
        System.out.println("El id de usuario es: " + account.getIdUser());
        System.out.println("La cantidad a actualizar es: " + account.getAmount());
        System.out.println("Los movimientos a actualizar son: " + account.getMovements());
        AccountServiceResponse result = this.accountService.partialUpdate(accountId, account);
        return new ResponseEntity<>(result,result.getHttpStatus());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAccount (@PathVariable String id) {
        System.out.println("deleteAccount");
        System.out.println("La cuenta recibida por parametro es: " + id);
        boolean deletedAccount = this.accountService.delete(id);
        return new ResponseEntity<>(deletedAccount ? "El producto se ha borrado" : "Producto no encontrado", deletedAccount ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
