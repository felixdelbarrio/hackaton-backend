package com.example.hackaton.back.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection="accounts")
public class AccountModel {

    @Id
    private String account;
    private String idUser;
    private float amount;
    private Map<String,Float> movements;

    public AccountModel() {
    }

    public AccountModel(String account, String idUser, float amount, Map<String, Float> movements) {
        this.account = account;
        this.idUser = idUser;
        this.amount = amount;
        this.movements = movements;
    }

    public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getIdUser() {
        return this.idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public float getAmount() {
        return this.amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Map<String, Float> getMovements() {
        return this.movements;
    }

    public void setMovements(Map<String, Float> movements) {
        this.movements = movements;
    }
}
