package com.example.hackaton.back.models;

import org.junit.Assert;
import org.junit.Test;

public class UserModelTest {

    @Test
    public void test_userModel_constructor() {
        UserModel sut = new UserModel();

        Assert.assertNotNull(sut);
        Assert.assertNull(sut.getId());
        Assert.assertNull(sut.getName());
        Assert.assertNull(sut.getPhone());
        Assert.assertNull(sut.getSurname());
    }

    @Test
    public void test_userModel_constructor_arguments() {
        UserModel sut = new UserModel("id", "name", "surname", "phone");

        Assert.assertNotNull(sut);
        Assert.assertEquals(sut.getId(), "id");
        Assert.assertEquals(sut.getName(), "name");
        Assert.assertEquals(sut.getSurname(), "surname");
        Assert.assertEquals(sut.getPhone(), "phone");
    }

    @Test
    public void test_userModel_setters_getters() {

        UserModel sut = new UserModel();

        sut.setId("id");
        sut.setName("name");
        sut.setSurname("surname");
        sut.setPhone("phone");

        Assert.assertNotNull(sut);
        Assert.assertEquals(sut.getId(), "id");
        Assert.assertEquals(sut.getName(), "name");
        Assert.assertEquals(sut.getSurname(), "surname");
        Assert.assertEquals(sut.getPhone(), "phone");
    }
}
